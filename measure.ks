//
//YOU NEED TO HAVE AN ACCELEROMETER ON YOUR CRAFT TO RUN THIS SCRIPT!
//This script can measure a bunch of stuff:
// - Cd*A	Once you know this value for a certain craft you can use it to calculate its drag.
//		Fall to through the atmosphere at a constant angle relative to the surface velocity (preferably retrograde) and start this script.
//		The value should stay more or less constant. Use the value it has in the upper atmosphere (>10km).
// - landedaltradar	When your vessel is landed the alt:radar value is still not 0, because the root part of the vessel is still at a certain
//			height above the ground. This value tells you what the alt:radar value is.
//			Have your vessel landed on the ground and run this script to determine the value.
// - droneshipaltradar	When your vessel is landed on a target (e.g. a droneship) the target has its own offset between its altitude and the altitude of its surface.
//			First have your vessel landed on land as normal and measure landedaltradar as described above. Then edit this script to
//			set the variable landedaltradar to the value you've measured before. After this you need to have your vessel landed on
//			your target and run this script again.
//			

local landedaltradar is 0. //If you want to measure droneshipaltradar you have to set this value to the landedaltradar of your craft

runpath("0:/predict.ks").
runpath("0:/flightstats.ks").

declare function measureCdTimesA {
	local dragmeasure is measureAcc() - gravitacc().
	local curdensity is density(ship:altitude, ship:body, "accurate").
	if curdensity = 0 {
		return 0.
	} else {
		return 2 * dragmeasure:mag * 1000 * ship:mass / (curdensity * ship:velocity:surface:sqrmagnitude).
	}
}

local prevvel is ship:velocity:orbit.
local prevveltime is time:seconds.
local curacc is v(0,0,0).
declare function measureAcc {
	if time:seconds > prevveltime + 0.01 {
		set curacc to (ship:velocity:orbit-prevvel)/(time:seconds-prevveltime).
		set prevvel to ship:velocity:orbit.
		set prevveltime to time:seconds.
	}
	return curacc.
}

clearscreen.
local measurelist is list().
until false {
	local curCdA is measureCdTimesA().
	if ship:altitude < 30000 and ship:verticalspeed < 0 {
		measurelist:add(curCdA).
	}

	set flightstats["Current Cd*A"] to round(curCdA, 1) + " m^2".
	if alt:radar < 1000 {
		local measurementsum is 0.
		for measurement in measurelist {
			set measurementsum to measurementsum + measurement.
		}
		if not measurelist:length = 0 {
			set flightstats["Average Cd*A"] to round(measurementsum/measurelist:length, 1) + " m^2".
		}
	} else {
		set flightstats["Average Cd*A"] to "measuring... Wait until altitude < 1 km.".
	}

	set flightstats["altradar"] to round(alt:radar, 2) + " m".
	set flightstats["landedaltradar"] to landedaltradar + " m".
	if hastarget {
		if landedaltradar = 0 {
			print "If you want to measure droneshipaltradar then please first land your vessel on land, measure landaltradar and enter the value into the script." at (0,1).
		} else {
			set flightstats["droneshipaltradar"] to round(alt:radar-landedaltradar - (target:altitude-max(target:geoposition:terrainheight,0)) , 2) + " m".
		}
	}
	printflightstats().
}
