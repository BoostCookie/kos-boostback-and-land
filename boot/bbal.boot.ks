@LAZYGLOBAL OFF.
switch to 0.
local processor is core:part:getmodule("KOSProcessor").
local landtarget is latlng(-0.2187, -74.5732).
clearscreen.
processor:doevent("Open Terminal").
print "booting...".
wait 3.
print "Waiting for staging.".
wait until hastarget or processor:part:decoupledin = -1.
wait 1.
if hastarget {
	set landtarget to target.
	print "Droneship selected.".
	print "Waiting for staging.".
	wait until processor:part:decoupledin = -1.
}
processor:doevent("Open Terminal").
runpath("0:/bbal.ks", 25.5, 32.67, 0.6, landtarget, 1.95).
