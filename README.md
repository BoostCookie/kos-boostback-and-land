# kOS boostback and land

This script works in Kerbal Space Program with the kOS mod installed.
It tries to boost back and land an orbital rocket booster, Falcon 9 style.

Tested in KSP 1.7.3 with kOS 1.1.9.

How to:

0. Add the mod kOS to your installation of KSP and copy the *.ks-files to YourKSPDirectory/Ships/Script/
1. Build a rocket (or use `Kalcon9_stock.craft` from here):
  * The Falcon 9 has 9 engines in the first stage, but only uses 3 for the boostback and 3 or 1 for the hoverslam. To reenact this you can set ActionGroup1 to the first 6 engines to be toggled and ActionGroup2 to the remaining 2. So after enabling ActionGroup1 and ActionGroup2 only the center engine is enabled.
  * Let the Gear-ActionGroup toggle the landing legs.
  * Put a kOS control system on it.
2. Do a few measurement tests with your booster:
  * Have the booster landed on the ground standing on its landing legs. Type into the kOS Console:
	```
	switch to 0.  
	run measure.ks.
	```
  * Look at the value of `altradar` and write it down. This is your `landedaltradar`.
  * Let the booster fall down through the atmosphere as it would after a boostback. Make sure to have it in a retrograde orientation. Type into the kOS Console:
	```
	switch to 0.  
	run measure.ks.  
	```
	Wait until you're lower than 1 km. That's when `Average Cd*A` has a value visible in the console. This is your value for `CdTimesA`.
  * This step is only necessary if you want to land on a droneship: Edit the file measure.ks with a text editor. Change the value of landedaltradar to the value you've measured before. So if you've measured 32.4 m before, change the line from `local landedaltradar is 0.` to `local landedaltradar is 32.4.`. Have your booster landed on your droneship, run `measure.ks` again and write down the value of `droneshipaltradar`.
3. Edit the file bbal.ks with a text editor and enter the values you've measured accordingly. Set `arg_landtarget` to the coordinates you want to land at. If you want to land on a droneship you can ignore this variable.
4. Launch your rocket. When your Apoapsis is above 80 km you can shut down the engines, start the second stage and point it horizontally. Switch back to the booster. If you want to land on a droneship you should now set it as target. Enter into the kOS-console:
	```
	switch to 0.  
	run bbal.ks.  
	```
	When the booster is finished with its boostback burn you can switch back to the second stage and get it into orbit. Then switch back to the booster for the reentry burn and the hoverslam. Again type into the console:
	```
	switch to 0.  
	run bbal.ks.  
	```
	Cross your fingers and pray to Jebediah.
5. ???
6. PROFIT!
