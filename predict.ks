@LAZYGLOBAL OFF.

//
//The function predictlandcoord() returns the geocoordinate
//where the craft will hit the ground.
//
global oceancount is 0.
global terraincount is 0.

runpath("0:/mathfunctions.ks").
global curlandcoord is latlng(0,0).
global landcoordevaltime is 0.
global landcoordevalduration is 0.
global landcoordspeed is 0.
//returns the geoposition of where your craft will be when it is at targetheight above the terrain
declare function predictlandcoord {
	parameter method is "precise", CdTimesA is 0, targetheight is 0, position is ship:position-ship:body:position, velocity is ship:velocity:orbit, refbody is ship:body.
	local prevlandcoordevaltime is landcoordevaltime.
	set landcoordevaltime to time:seconds.
	local newgeocoord is latlng(0,0).
	//does a stepwise simulation until the craft hits the targetheight
	//takes about half a second to compute
	//the only method here that takes drag into account
	if method = "precise" {
		local simtime is 0.
		until position:mag < refbody:radius + max(0,newgeocoord:terrainheight) + targetheight {
			local dragacc is dragforce(CdTimesA, position:mag-refbody:radius, velocity - vcrs(refbody:angularvel, position), refbody)/ship:mass.
			//use kepler outside of the atmosphere
			if dragacc:mag = 0 {
				local newposveltime is posveltimeataltitude(49999).
				set position to newposveltime[0].
				set velocity to newposveltime[1].
				set simtime to simtime + newposveltime[2].
			//simulate stepwise inside of the atmosphere
			} else {
				local timestep is 8.
				set simtime to simtime + timestep.
				local accres is gravitacc(position, refbody) + dragacc.
				set position to accres / 2 * timestep * timestep + velocity * timestep + position.
				set velocity to accres * timestep + velocity.
				//for the geocoordinates, take the rotation of the planet into account
				set newgeocoord to convertPosvecToGeocoord(r(0, refbody:angularvel:mag * simtime * constant:RadToDeg, 0) * position).
			}
		}
	//uses the orbit's ellipse to calculate where the ship will hit the planet
	} else if method = "kepler" {
		//assume the terrain is always as high as it is now
		set newgeocoord to convertPosvecToGeocoord(position).
		local geoheight is max(0,newgeocoord:terrainheight).
		local newposveltime is posveltimeataltitude(geoheight+targetheight).
		local newpos is newposveltime[0].
		local deltaT is newposveltime[2].
		//let the planet rotate and see how high the terrain actually is
		set newgeocoord to convertPosvecToGeocoord(r(0,refbody:angularvel:mag*deltaT*constant:RadToDeg,0) * newpos).
		set geoheight to max(0,newgeocoord:terrainheight).
		set newposveltime to posveltimeataltitude(geoheight+targetheight).
		set newpos to newposveltime[0].
		set deltaT to newposveltime[2].
		set newgeocoord to convertPosvecToGeocoord(r(0,refbody:angularvel:mag*deltaT*constant:RadToDeg,0) * newpos).
	//assumes the planet is flat, gravity is constant and drag does not exist
	} else if method = "flat" {
		//assume the terrain is always as high as it is now
		set newgeocoord to convertPosvecToGeocoord(position).
		local geoheight is max(0,newgeocoord:terrainheight).
		//assume the average gravity is the same as the gravity at avgaltfactor*heighttofall
		local avgaltfactor is 0.
		local avgposition is (avgaltfactor*position:mag + (1-avgaltfactor)*(refbody:radius+geoheight+targetheight))*position:normalized.
		local avggravity is gravitacc(avgposition, refbody).
		//the time it would take in the vacuum
		local v_vert is -avggravity:normalized*velocity.
		local deltaT is (v_vert + sqrt(v_vert*v_vert + 2*avggravity:mag*(position:mag-refbody:radius-geoheight-targetheight)))/avggravity:mag.
		//update the height of the terrain based on a new estimation of the landing spot
		local newpos is avggravity / 2 * deltaT*deltaT + velocity*deltaT + position.
		set newgeocoord to convertPosvecToGeocoord(r(0,refbody:angularvel:mag*deltaT*constant:RadToDeg,0) * newpos).
		set geoheight to max(0,newgeocoord:terrainheight).
		//update the avggravity based on this new estimation
		set avgposition to (avgaltfactor*position:mag + (1-avgaltfactor)*(refbody:radius+geoheight+targetheight))*position:normalized.
		set avggravity to gravitacc(avgposition, refbody).
		//the time to get to the altitude of avgposition
		set v_vert to -avggravity:normalized*velocity.
		set deltaT to (v_vert + sqrt(v_vert*v_vert + 2*avggravity:mag*(position-avgposition):mag))/avggravity:mag.
		//correct the directions of the vectors
		set avgposition to avggravity / 2 * deltaT*deltaT + velocity * deltaT + position.
		set avggravity to gravitacc(avgposition, refbody).
		//correct the vertical velocity
		set v_vert to -avggravity:normalized*velocity.
		//the time it takes to get to the ground
		local deltaT is (v_vert + sqrt(v_vert*v_vert + 2*avggravity:mag*(position:mag-refbody:radius-geoheight-targetheight)))/avggravity:mag.
		set newpos to avggravity / 2 * deltaT*deltaT + velocity*deltaT + position.
		set newgeocoord to convertPosvecToGeocoord(r(0,refbody:angularvel:mag*deltaT*constant:RadToDeg,0) * newpos).
	} else {
		print "ERROR: No method '" + method + "' for predictlandcoord".
		return -1.
	}
	set landcoordevalduration to time:seconds - landcoordevaltime.
	local prevlandcoord is curlandcoord.
	set curlandcoord to newgeocoord.
	if method = "precise" {
		set landcoordspeed to (curlandcoord:position-prevlandcoord:position):mag/(landcoordevaltime-prevlandcoordevaltime).
	}
	return curlandcoord.
}

//function uses kepler to calculate the posvec, velvec at a certain altitude (past apoapsis) and the time it takes to get there
declare function posveltimeataltitude {
	parameter targetalt, startpos is ship:position-ship:body:position, startvel is ship:velocity:orbit, refbody is ship:body.
	//this is all based on this equation for the radius of an ellipse:
	//r(phi) = r0/(1+ecc*cos(phi))
	//where r0 is the radius the orbit with the same angular momentum would have it it were circular
	//and ecc is the eccentricity of the ellipse

	//(angular momentum)/mass is a constant vector
	local rcrossv is vcrs(startpos, startvel).
	//get r0 by the angular momentum
	local r0 is rcrossv:sqrmagnitude / refbody:mu.
	//ecc = sqrt(1+E/E0) with E="Energy of the orbit" and E0=GMm/(2r0)
	local ecc is sqrt(1 + r0 * (startvel:sqrmagnitude/refbody:mu - 2/startpos:mag)).
	//the calculation of the startangle depends on whether we're past apoapsis or not
	local pastap is false.
	if startpos*startvel < 0 { set pastap to true. }
	//the angle between periapsis and startpos
	//(inverse of the ellipsis function)
	local startphi is (arccos(1/ecc * (r0/startpos:mag -1))).
	if pastap { set startphi to 360 - startphi. }
	//the angle between periapsis and targetposition where altitude=targetalt
	local targetphi is 360-arccos(1/ecc * (r0/(refbody:radius+targetalt) -1)).
	local deltaphi is targetphi - startphi.
	//rotate the current position by deltaphi to get the direction of targetpos
	local targetpos is (angleaxis(deltaphi, rcrossv) * startpos):normalized * (refbody:radius+targetalt).

	//calculate the magnitude of the targetvelocity by conservation of energy
	//E = 0.5*m*v^2 - GMm/r = const.
	local targetvelmag is sqrt(startvel:sqrmagnitude + 2*refbody:mu*(1/targetpos:mag-1/startpos:mag)).
	//calculate the angle between targetpos and targetvel by conservation of angular momentum
	//L=m*(rxv) <=> |L|=m*|r|*|v|*sin(phi)=const
	local targetposvelangle is 180-arcsin(rcrossv:mag/targetpos:mag/targetvelmag).
	//rotate the targetpos by this angle to get the direction of targetvel
	local targetvel is (angleaxis(targetposvelangle, rcrossv) * targetpos):normalized * targetvelmag.

	//calculate the relevant part of the area of the ellipse
	local partarea is ellipsearea(targetphi, ecc, r0) - ellipsearea(startphi, ecc, r0).
	//calculate the semi-major axis a
	local a is r0/(1-ecc*ecc).
	//use Kepler3 to calculate the time it takes to get to targetpos
	//time/partarea = orbitperiod/fullarea
	local time is 2*partarea/sqrt((1-ecc*ecc)*a*refbody:mu).

	return list(targetpos, targetvel, time).
}
//calculates the area from periapsis to phi
declare function ellipsearea {
	parameter phi, ecc, r0.
	local k1 is 1-ecc*ecc.
	local k2 is ecc-1.
	local ta is tan(phi/2).
	//the area integral of an ellipsis
	//with rvec = r(phi) * (cos(phi), sin(phi), 0)^T
	//calculated with geogebra, because wolframalpha was giving me a non continuous function
	return r0*r0/k1 * ((constant:pi*floor(phi/360+0.5)-arctan(ta*k2/sqrt(k1))*constant:degtorad)/sqrt(k1) + ecc*ta/(k2*(ta*ta-1)-2)).
}

local testing is false.
if testing {
	clearscreen.
	runpath("0:/flightstats.ks").
	local CdTimesA is 25.5.
	setarrow("kepler", { local coord is predictlandcoord("kepler"). return coord:altitudeposition(max(0,coord:terrainheight)). }).
	setarrow("flat", { local coord is predictlandcoord("flat"). return coord:altitudeposition(max(0,coord:terrainheight)). }).
	setarrow("tr", { local coord is addons:tr:impactpos. return coord:altitudeposition(max(0,coord:terrainheight)). }).
	set addons:tr:retrograde to true.
	until false {
		set flightstats["testing"] to testing.
		set flightstats["altitude"] to round(ship:altitude) + " m".
		local arrowtime is time:seconds.
		//from {local x is 0.} until x=10 step {set x to x+1.} do {
		refresharrows().//}
		set flightstats["arrowtime"] to round(time:seconds-arrowtime, 3) + " s".
		set flightstats["density"] to round(density(ship:altitude, ship:body, "accurate"), 3) + " kg/m^3".
		set flightstats["quick density"] to round(density(ship:altitude, ship:body, "quick"), 3) + " kg/m^3".
		set flightstats["dragacc"] to round(dragforce(CdTimesA):mag/ship:mass, 2) + " m/s^2".
		set flightstats["gravitacc"] to round(gravitacc():mag) + " m/s^2".
		local trcoord is addons:tr:impactpos.
		local trpos is trcoord:altitudeposition(max(0,trcoord:terrainheight)).
		local keplercoord is predictlandcoord("kepler").
		local keplerpos is keplercoord:altitudeposition(max(0,keplercoord:terrainheight)).
		local flatcoord is predictlandcoord("flat").
		local flatpos is flatcoord:altitudeposition(max(0,flatcoord:terrainheight)).
		local precisecoord is predictlandcoord("precise", CdTimesA).
		local precisepos is precisecoord:altitudeposition(max(0,precisecoord:terrainheight)).
		set flightstats["keplererror"] to round((trpos-keplerpos):mag) + " m".
		set flightstats["flaterror"] to round((trpos-flatpos):mag) + " m".
		set flightstats["preciseerror"] to round((trpos-precisepos):mag) + " m".
		printflightstats().
	}
}

declare function centrifugalacc {
	parameter position is ship:position - ship:body:position, refbody is ship:body.
	return vcrs(refbody:angularvel, vcrs(position, refbody:angularvel)).
}

declare function coriolisacc {
	parameter velocity is ship:velocity:surface, refbody is ship:body.
	return 2 * vcrs(velocity, refbody:angularvel).
}

declare function gravitacc {
	parameter position is ship:position - ship:body:position, refbody is ship:body.
	return -refbody:mu * position:normalized / position:sqrmagnitude.
}

declare function density {
	parameter height is ship:altitude, refbody is ship:body, method is "quick".
	if method = "quick" {
		set height to height/1000. //altitude in km
		if refbody:name = "Kerbin" {
			if height > 50 { return 0.
			} else if height > 40 { return -0.0001*height+0.005.
			} else if height > 30 { return -0.0005*height+0.021.
			} else if height > 25 { return -0.0018*height+0.06.
			} else if height > 20 { return -0.005*height+0.14.
			} else if height > 15 { return -0.0136*height+0.312.
			} else if height > 10 { return -0.036*height+0.648.
			} else if height > 7.5 { return -0.0632*height+0.92.
			} else if height > 5 { return -0.0784*height+1.034.
			} else if height > 2.5 { return -0.1024*height+1.154.
			} else { return -0.1308*height+1.225. }
		} else {
			print "Error. Body is not Kerbin.".
			return "Error. Body is not Kerbin.".
		}
	} else if method = "accurate" {
		if refbody:name = "Kerbin" {
			if height > 68798 { return 0. }
			//Convert the altitude to values for Earth
			//Formula: https://wiki.kerbalspaceprogram.com/wiki/Kerbin#Atmosphere
			local h is 7.96375 * height / (6371 + 0.00125 * height). //km
			//density [kg/m^3] = k * pressure [Pa]
			local k is 0.
			//k = 1/(R*T). R is the specific gas constant
			local R is 287.053. //J/kg-K
			//Temperature T dependends on altitude:
			//Equations: http://www.braeunig.us/space/atmmodel.htm#table4
			if h > 84.852 { set k to 0.
			} else if h > 71 { set k to 1/(R*(356.65-2*h)).
			} else if h > 51 { set k to 1/(R*(413.45-2.8*h)).
			} else if h > 47 { set k to 1/(R*270.65).
			} else if h > 32 { set k to 1/(R*(139.05+2.8*h)).
			} else if h > 20 { set k to 1/(R*(196.65+h)).
			} else if h > 11 { set k to 1/(R*216).
			} else { set k to 1/(R*(288.15-6.5*h)). }
			return k * refbody:atm:altitudepressure(height) * constant:AtmToKPa * 1000.
		} else {
			print "Error. Body is not Kerbin.".
			return "Error. Body is not Kerbin.".
		}
	}
}

declare function dragforce {
	parameter CdTimesA, height is ship:altitude, velocity is ship:velocity:surface, refbody is ship:body.
	return -density(height, refbody) * velocity:normalized * velocity:sqrmagnitude * CdTimesA / 2000.
}
